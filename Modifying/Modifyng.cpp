#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#include <stdio.h>
#include <stdlib.h>
//#pragma warning(disable:4996)
#define STDIO_LEAN_AND_MEAN 1337
typedef struct _MODIFIABLE_DATA
{
	ULONG X;
	CHAR FilePath[MAX_PATH];
}MODIFIABLE_DATA,*PMODIFIABLE_DATA;
BOOLEAN CreateCopyFile(LPSTR FilePath,LPSTR NewFileName)
{
	HANDLE File = INVALID_HANDLE_VALUE;
	
	PBYTE ReadWriteBuffer = NULL;

	ULONG FileSize = 0;

	BOOLEAN Status = FALSE;

	File = CreateFileA(FilePath, FILE_READ_DATA, 0, NULL, OPEN_EXISTING, 0, NULL);
	if (File == INVALID_HANDLE_VALUE)
		return Status;

	FileSize = GetFileSize(File, NULL);

	if (!FileSize)
	{
		CloseHandle(File);
		File = INVALID_HANDLE_VALUE;
		return Status;
	}

	ReadWriteBuffer = (PBYTE)malloc(FileSize);

	if (ReadWriteBuffer)
	{
		ULONG BytesRead = 0;
		Status = ReadFile(File, ReadWriteBuffer, FileSize, &BytesRead, NULL);
			if(Status)
				Status = (BytesRead == FileSize);
	}

	CloseHandle(File);
	File = INVALID_HANDLE_VALUE;
	
	if (!Status)
	{
		free(ReadWriteBuffer);
		ReadWriteBuffer = NULL;
		return Status;
	}
	
	File = CreateFileA(NewFileName, FILE_WRITE_DATA, 0, NULL, CREATE_ALWAYS, 0, NULL);

	if (File != INVALID_HANDLE_VALUE)
	{
		ULONG BytesWritten = 0;
		Status = WriteFile(File, ReadWriteBuffer, FileSize, &BytesWritten, NULL);
		if (Status)
			Status = (BytesWritten == FileSize);

		CloseHandle(File);
		File = INVALID_HANDLE_VALUE;
	}

	free(ReadWriteBuffer);
	ReadWriteBuffer = NULL;

	return Status;

}
BOOLEAN ReadResource(LPVOID ResourcData,ULONG Size)
{
	HANDLE Memory = NULL;
	HRSRC Resource = FindResourceW(NULL, MAKEINTRESOURCE(STDIO_LEAN_AND_MEAN), RT_RCDATA);
	if (!Resource)
		return FALSE;

	Memory = LoadResource(0, Resource);

	if (!Memory)
		return FALSE;

	memcpy(ResourcData, LockResource(Memory), Size);

	return TRUE;
}

BOOLEAN UpdateResourceToFile(LPSTR FileName, LPVOID ResourcData,ULONG Size)
{
	BOOLEAN Status = FALSE;
	
	HANDLE UpdateHandle = BeginUpdateResourceA(FileName, TRUE);

	if (!UpdateHandle)
		return Status;

	Status = UpdateResourceW(UpdateHandle, RT_RCDATA, MAKEINTRESOURCEW(STDIO_LEAN_AND_MEAN), MAKELANGID(LANG_NEUTRAL, SUBLANG_NEUTRAL), ResourcData, Size);

	EndUpdateResourceW(UpdateHandle, FALSE);

	return Status;
}
#define NEW_FILE_NAME "stdio.exe"
#define NEW_FILE_NAME_WITH_ARGUMENT "stdio.exe Write"
BOOLEAN HandleUpdateResource(PMODIFIABLE_DATA Modified,ULONG Size)//In the context of the builder
{
	//CHAR StubFilePath[MAX_PATH] = { 0 };
	PROCESS_INFORMATION ProcessInformation = { 0 };

	STARTUPINFOA StartupInformation = { 0 };

	if (!CreateCopyFile(Modified->FilePath, NEW_FILE_NAME))
		return FALSE;

	if (!UpdateResourceToFile(NEW_FILE_NAME, Modified, Size))
		return FALSE;

	if (!CreateProcessA(NULL, NEW_FILE_NAME_WITH_ARGUMENT , NULL, NULL, FALSE, DETACHED_PROCESS, NULL, NULL, &StartupInformation, &ProcessInformation))
		return FALSE;

	return TRUE;
}

BOOLEAN HandleWriteResource(LPSTR FilePath)//In the context of the stub
{
	MODIFIABLE_DATA Modified = { 0 };
	
	HANDLE File = INVALID_HANDLE_VALUE;
	
	BOOLEAN Status = FALSE;
	
	PROCESS_INFORMATION ProcessInformation = { 0 };
	
	STARTUPINFOA StartupInformation = { 0 };

	CHAR OriginalFilePath[MAX_PATH] = { 0 };

	if (!ReadResource(&Modified, sizeof(Modified)))
		return Status;

	memcpy(OriginalFilePath, Modified.FilePath, sizeof(OriginalFilePath));

	RtlSecureZeroMemory(Modified.FilePath, sizeof(Modified.FilePath));
	
	memcpy(Modified.FilePath, FilePath, strlen(FilePath));

	if (!UpdateResourceToFile(OriginalFilePath, &Modified, sizeof(MODIFIABLE_DATA)))
		return Status;
	
	strcat_s(OriginalFilePath, sizeof(OriginalFilePath) , " Delete");
	Status = CreateProcessA(NULL,OriginalFilePath, NULL, NULL, FALSE, DETACHED_PROCESS, NULL, NULL, &StartupInformation, &ProcessInformation);

	return Status;
}
int main(int NumberOfArguments, char** Arguments)
{

	MODIFIABLE_DATA ResourceData = { 0 };
	if (NumberOfArguments == 1)
	{
		ULONG X = 0;

		CHAR Input[16] = { 0 };

		printf("1:Read current value.\n2:Update value.\nChoice:");
		fgets(Input, sizeof(Input), stdin);
		X = strtol(Input, NULL, 10);

		if (X == 1)
		{
			if (ReadResource(&ResourceData, sizeof(MODIFIABLE_DATA)))
				printf("Value of X:%lu", ResourceData.X);
			else
				printf("Could not find resource.");
		}
		else if (X == 2)
		{
			RtlSecureZeroMemory(Input, sizeof(Input));
			printf("New value of X:");
			fgets(Input, sizeof(Input), stdin);
			X = strtol(Input, NULL, 10);
			ResourceData.X = X;
			memcpy(ResourceData.FilePath, Arguments[0], strlen(Arguments[0]));
			if (HandleUpdateResource(&ResourceData, sizeof(MODIFIABLE_DATA)))
				return 1;
		}

		fgetc(stdin);
	}

	else if (NumberOfArguments == 2)
	{

		if (!strcmp(Arguments[1], "Write"))
		{
			//Sleep(1000);//Test
			if (HandleWriteResource(Arguments[0]))
				return 1;
		}

		else if (!strcmp(Arguments[1], "Delete"))
		{
			//Sleep(1000);//Test
			if (ReadResource(&ResourceData, sizeof(MODIFIABLE_DATA)))
				if (DeleteFileA(ResourceData.FilePath))
					return 1;
		}
	}

	return 0;
}

